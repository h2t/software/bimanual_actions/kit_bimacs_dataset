#!/usr/bin/env python

from distutils.core import setup

setup(name='kit_bimacs_dataset',
      version='1.3.1',
      description='KIT Bimanual Actions Dataset',
      author='Christian R. G. Dreher',
      author_email='c.dreher@kit.edu',
      url='https://gitlab.com/dreher/kit_bimacs_dataset',
      packages=['bimacs'],
)
